# NMAPPER

## Description

`nmap` wrapper which launchs sequentially a bunch of `nmap` commands:

Hostname detection through SSL cert parsing on port 443 is also added (helpful 
for a bunch of HTB boxes).


```
USAGE
=====
    nmaper [-a MIN-RATE] [-h] [-s] [-r] [-t] [-u] [-x] [-o OUTPUT_FOLDER] 
        -i target_ip

DESCRIPTION
===========
Wrapper script around NMAP.

With only the mandatory '-i' option, the script will perform the below 
actions sequentially:

- Fast TCP scan (100 most common TCP ports)
- Regulard TCP scan (1000 most common TCP ports)
- Thorough TCP scan (all 65535 TCP/ports)
- Fast UDP port scan (100 most common UDP ports)
- Service fingerprinting and run some NSE scripts

It is however possible to launch a predefine scan. See the options.

OPTIONS
=======
    -a <min_rate>       nmap --min-rate option (Packets per second). On 
HackTHeBox or other CTF, you can go very high (e.g. 5000+) without risking
crashing most services. (Default: 200)
    -h                  Print this help
    -i <target_ip>      Target IP
    -o <output_folder>  Output folder name
    -r                  Launch a regular TCP scan (top 1000 common ports)
    -s                  Launch a short TCP scan (top 100 common ports)
    -t                  Launch a thorough TCP scan (all 65535 ports)
    -u                  Launch a short UDP scan (top 100 ports)
    -x                  Skip service fingerprinting and NSE scripts

AUTHOR
=======
@0nemask - 2022

```

Example output: 

![HTB output](./img/nmaper_sudo_all.png)

### Output folder

Output folder structure example:

```
nmaper-redcross
├── 00-nmaper-10.129.2.8.log
├── nmap
│   ├── nmap-10.129.2.8-full-tcp.gnmap
│   ├── nmap-10.129.2.8-full-tcp.nmap
│   ├── nmap-10.129.2.8-full-tcp.xml
│   ├── nmap-10.129.2.8-regular-tcp.gnmap
│   ├── nmap-10.129.2.8-regular-tcp.nmap
│   ├── nmap-10.129.2.8-regular-tcp.xml
│   ├── nmap-10.129.2.8-short-tcp.gnmap
│   ├── nmap-10.129.2.8-short-tcp.nmap
│   ├── nmap-10.129.2.8-short-tcp.xml
│   ├── nmap-10.129.2.8-short-udp.gnmap
│   ├── nmap-10.129.2.8-short-udp.nmap
│   └── nmap-10.129.2.8-short-udp.xml
└── ssl
    └── 10.129.2.8_443.crt

2 directories, 14 files

```

## Installation

Just clone the repo and launch the script with an IP to scan

```bash
git clone https://gitlab.com/onemask/nmaper
cd nmaper
chmod +x nmaper.sh
./nmaper.sh -i <target_ip>
```

You can also create a symbolic link to ease running the tool:

```bash
# From within the repo
sudo ln -s $(pwd)/nmaper.sh /usr/local/bin/nmaper
sudo chmod +x /usr/local/bin/nmaper
```

## Contributions

Feel free to report any issues or push any relevant fixes/improvments!
